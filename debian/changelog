libmojolicious-plugin-renderfile-perl (0.12-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libmojolicious-perl.
    + libmojolicious-plugin-renderfile-perl: Drop versioned constraint on
      libmojolicious-perl in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 28 Aug 2022 14:55:15 +0100

libmojolicious-plugin-renderfile-perl (0.12-4) unstable; urgency=medium

  * debian/control
    - Set debhelper-compat (= 13)
    - Set Standards-Version: 4.5.0
    - Add Rules-Requires-Root: no
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Hideki Yamane <henrich@debian.org>  Wed, 03 Jun 2020 00:35:29 +0900

libmojolicious-plugin-renderfile-perl (0.12-3) unstable; urgency=medium

  * add debian/salsa-ci.yml
  * debian/control
    - set Standards-Version: 4.4.0
    - drop debhelper, use debhelper-compat is enough

 -- Hideki Yamane <henrich@debian.org>  Sun, 29 Sep 2019 11:36:13 +0900

libmojolicious-plugin-renderfile-perl (0.12-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Hideki Yamane ]
  * debian/{compat,control}
    - use dh12
  * debian/control
    - update Vcs-* to use salsa.debian.org
    - set Standards-Version: 4.3.0
    - add Testsuite: autopkgtest-pkg-perl
  * debian/copyright
    - update copyright year

 -- Hideki Yamane <henrich@debian.org>  Sun, 14 Apr 2019 22:57:58 +0900

libmojolicious-plugin-renderfile-perl (0.12-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 4.1.1 with no change

 -- Hideki Yamane <henrich@debian.org>  Sat, 30 Sep 2017 22:13:15 +0900

libmojolicious-plugin-renderfile-perl (0.11-3) unstable; urgency=medium

  * debian/control
    - set Maintainer: Debian Perl Group
    - set Standards-Version: 4.1.0
    - add Vcs-*

 -- Hideki Yamane <henrich@debian.org>  Sat, 26 Aug 2017 08:09:09 +0900

libmojolicious-plugin-renderfile-perl (0.11-2) unstable; urgency=medium

  * debian/rules
    - Don't install README (Closes: #839621)
      Thanks to gregor herrmann <gregoa@debian.org>
  * debian/compat
    - set 10
  * debian/watch
    - update to version 4
  * debian/control
    - set Standards-Version: 4.0.0

 -- Hideki Yamane <henrich@debian.org>  Tue, 18 Jul 2017 06:30:45 +0900

libmojolicious-plugin-renderfile-perl (0.11-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 17 Apr 2017 23:06:17 +0900

libmojolicious-plugin-renderfile-perl (0.10-1) unstable; urgency=low

  * Initial Release.

 -- Hideki Yamane <henrich@debian.org>  Thu, 22 Sep 2016 16:15:06 +0900
